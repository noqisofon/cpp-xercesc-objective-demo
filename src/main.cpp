#include <memory>
#include <string>
#include <iostream>

#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>

using namespace xercesc;


int main(int argc, char *argv[]) {
    try {
        XMLPlatformUtils::Initialize();
    } catch ( const XMLException& ex ) {
        char *message = XMLString::transcode( ex.getMessage() );

        std::cerr << "Error during initialize! :" << std::endl
                  << message << std::endl;

        XMLString::release( &message );
        
        return 1;
    }

    std::shared_ptr<XercesDOMParser> parser        =  std::shared_ptr<XercesDOMParser>( new XercesDOMParser() );

    parser->setValidationScheme( XercesDOMParser::Val_Always );
    parser->setDoNamespaces( true );

    std::shared_ptr<ErrorHandler>    error_handler = std::shared_ptr<ErrorHandler>( new HandlerBase() );

    parser->setErrorHandler( error_handler.get() );

    if ( argc > 1 ) {
        std::string                      xml_fileaname( argv[1] );

        try {
            parser->parse( xml_fileaname.c_str() );
        } catch ( const XMLException &xmlex ) {
            char *message = XMLString::transcode( xmlex.getMessage() );

            std::cerr << "exception message is:" << message << std::endl;

            XMLString::release( &message );

            return -1;
        } catch ( const DOMException &domex ) {
            char *message = XMLString::transcode( domex.getMessage() );

            std::cerr << "exception message is:" << message << std::endl;

            XMLString::release( &message );

            return -1;
        } catch ( const SAXParseException &saxpex ) {
            char *message = XMLString::transcode( saxpex.getMessage() );

            std::cerr << "exception message is:" << message << std::endl;

            XMLString::release( &message );

            return -1;
        } 
    }

    /*
     * std::shared_ptr<?> はプログラムが終了した際に持っているリソースを削除する。
     * しかしながら、ここで XMLPlatformUtils::Terminate() を呼んでしまうと、 terminate() の後に
     * リソースが開放される形になってしまってセグフォを起こす。
     * このため、XMLPlatformUtils::Terminate() を呼んでいない。
     *
     * XMLPlatformUtils::Terminate() を呼ばなくても支障は無いように思われるが、詳しくは不明。
     */
    //     XMLPlatformUtils::Terminate();

    return 0;
}
